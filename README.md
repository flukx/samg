Space Alert Mission Generator 2
===============================

With this project you are able to create your own random and custom Space
Alert missions. You need the following prerequisites:

* bash
* python (at least 3.7 or otherwise the dataclasses module)
* sox
* oggenc
* debianutils (for tempfile)

To create a new random mission issue the following command in a shell.

./samg.sh

The new ogg file will be put into the folder archive/.

Details of operation
--------------------

The SAMG2 consists of three parts: mission-planner.py, mission-scripter.py
and mission-executor.sh.

* mission-planner: This takes a config file (default is config.cfg) and
  produces a json file describing a new random mission.

* mission-scripter: This takes a json file and produces a bash script which
  will do the actual work regarding audio manipulation (by invoking sox).

* mission-executor: This invokes the bash script and archives the so created
  audio file in the archive/ folder. (This is the major time-sink.)

You can manually create a json-file and thus create your own custom missions
circumventing the mission-planner.

License
-------

The GPLv3 license applies to all code, but not the sound snippets which are
assets from the original game and incorporated under fair use policy.

Installation of prerequisites on Ubuntu
---------------------------------------

Ubuntu packages that include
* sox: sox
* oggenc: vorbis-tools

Python module `dataclasses`:
* pip3 install dataclasses
