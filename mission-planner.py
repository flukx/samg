#!/usr/bin/env python3

import argparse
import configparser
import copy
import itertools
import json
import random
from typing import Sequence, Tuple, Any, Mapping

from common import (
    MARK, SoundRepository, Threat, ThreatKind, Event, CommFail, DataTransfer,
    IncomingData, PhaseTransition, PhaseWarning,
    SoundKind as SK)

DEBUG = False


def randomgiven(valueshares: Sequence[Tuple[Any, int]],
                num: int) -> Sequence[Any]:
    """Random distribution according to values and shares as passed. Give num
    many distinct elements back.

    example: valueshares == [(1,1), (2,3), (3,1)] for randombiased(1,3).
    """
    assert(sum(x for _, x in valueshares) > 0)
    assert(len(valueshares) >= num)
    ret = []
    for i in range(num):
        tmp = []
        for item, times in valueshares:
            if item not in ret:
                tmp.extend(times*(item,))
        ret.append(random.choice(tmp))
    return ret


def randomgivenone(valueshares: Sequence[Tuple[Any, int]]) -> Any:
    return randomgiven(valueshares, 1)[0]


def randombiased(lower: float, upper: float) -> int:
    """
    Random distribution which is biased towards the mean. The edge cases
    have one probability share and every step away from the edge gains you
    two probability shares.

                 X
                 X
               X X X
               X X X
    Shares   X X X X X

    Values   5 6 7 8 9

    and

               X X
               X X
    Shares   X X X X

    Values   5 6 7 8
    """
    upper = int(upper)
    lower = int(lower)
    assert(upper >= lower)
    num = upper - lower + 1
    tmp = tuple(1 + 2*i for i in range(num // 2))
    if num % 2:
        shares = tmp + (num,) + tuple(reversed(tmp))
    else:
        shares = tmp + tuple(reversed(tmp))
    valueshares = tuple(zip(range(lower, upper + 1), shares))
    return randomgivenone(valueshares)


class Config:
    def __init__(self, path):
        config = configparser.ConfigParser()
        config.read(path)
        int_items = (
            "threatpoints", "unconfthreatpoints", "mintotalcomfail",
            "maxtotalcomfail", "minsinglecomfail", "maxsinglecomfail",
            "mintotalduration", "maxtotalduration", "normalwaketurns",
            "seriouswaketurns", "internaldelay", "normalwaketime",
            "seriouswaketime", "firstthreattime", "protectedtime")
        for name in int_items:
            setattr(self, name, config.getint("conf", name))
        valueshare_items = (
            "incidenttimes", "serious", "internal", "seriousinternal",
            "incomingdata", "datatransfer")
        for name in valueshare_items:
            self.getvalueshares(config, name)
        self.maxthreatpointsinturn = (0,) + tuple(
            int(x) for x in config.get(
                "conf", "maxthreatpointsinturn").split(','))
        self.phasethreatturns = (0,) + tuple(
            int(x) for x in config.get("conf", "phasethreatturns").split(','))
        self.minphaseduration = {}
        self.maxphaseduration = {}
        for phase in (1, 2, 3):
            self.minphaseduration[phase] = config.getint(
                "conf", "minphase{}duration".format(phase))
            self.maxphaseduration[phase] = config.getint(
                "conf", "maxphase{}duration".format(phase))

    def getvalueshares(self, config: configparser.ConfigParser,
                       name: str) -> None:
        values = tuple(
            int(x) for x in config.get("conf", name + "values").split(','))
        shares = tuple(
            int(x) for x in config.get("conf", name + "shares").split(','))
        setattr(self, name + "valueshares", tuple(zip(values, shares)))


EVENTDURATIONS = {}
EVENTDURATIONS['operation_start'] = 7
EVENTDURATIONS['phase_transition'] = 12
EVENTDURATIONS['operation_end'] = 8
EVENTDURATIONS['phase_warning'] = 3
EVENTDURATIONS['threat'] = 10
EVENTDURATIONS['serious_threat'] = 11
EVENTDURATIONS['unconf_threat'] = 13
EVENTDURATIONS['unconf_serious_threat'] = 14
EVENTDURATIONS['incoming_data'] = 5
EVENTDURATIONS['comm_fail'] = 5
EVENTDURATIONS['data_transfer'] = 14


def try_repeatedly(fun, maxtries=42):
    tries = 0
    while True:
        try:
            return fun()
        except Exception as e:
            if tries < maxtries:
                tries += 1
            else:
                raise e


class Planner:
    def __init__(self, conf: Config, repository: SoundRepository) -> None:
        self.conf = conf
        self.repository = repository
        self.plan = []
        self.free = []

    def makeaplan(self) -> None:
        self.conf.totalduration = randombiased(
            self.conf.mintotalduration, self.conf.maxtotalduration)

        def do_phasedurations():
            self.conf.phaseduration = {}
            self.conf.phaseduration[1] = randombiased(
                self.conf.minphaseduration[1], self.conf.maxphaseduration[1])
            self.conf.phaseduration[2] = randombiased(
                self.conf.minphaseduration[2], self.conf.maxphaseduration[2])
            self.conf.phaseduration[3] = (
                self.conf.totalduration - (self.conf.phaseduration[1]
                                           + self.conf.phaseduration[2]))
            if (self.conf.minphaseduration[3] > self.conf.phaseduration[3]
                    or (self.conf.phaseduration[3]
                        > self.conf.maxphaseduration[3])):
                raise ValueError("Invalid phase durations")
        try_repeatedly(do_phasedurations)

        if DEBUG:
            print("phasedurations: {}".format(self.conf.phaseduration))

        try_repeatedly(self.buildchallenge)

        print("Make json")
        self.makejson()

    def eligible_serious(self, turns: Sequence[int],
                         exclude: Sequence[int] = None):
        exclude = exclude or set()
        relevant = list(set(turns) - set(exclude))
        relevant.sort()
        ret = []
        for x, y in zip(relevant, relevant[1:] + [42]):
            if y - x > self.conf.seriouswaketurns:
                ret.append(x)
        return ret

    def buildchallenge(self):
        self.free = [True for i in range(self.conf.totalduration + 60)]
        self.plan = []

        self.setupphases()

        self.conf.threatcount = {}

        def determine_threat_count():
            tc = self.conf.threatcount
            serious = randomgivenone(self.conf.seriousvalueshares)
            tc['serious_internal'] = randomgivenone(
                self.conf.seriousinternalvalueshares)
            if tc['serious_internal'] > serious:
                raise ValueError("Too many serious internal")
            tc['serious_external'] = serious - tc['serious_internal']
            tc['internal'] = (randomgivenone(self.conf.internalvalueshares)
                              - 2*tc['serious_internal'])
            if tc['internal'] < 0:
                raise ValueError("Too few internal threat points")
            tc['external'] = (
                self.conf.threatpoints - (2*tc['serious_external']
                                          + tc['internal']
                                          + 2*tc['serious_internal']))
            if tc['external'] < 0:
                raise ValueError("Too few threat points")
        try_repeatedly(determine_threat_count)

        if DEBUG:
            print("threatcount = {}".format(self.conf.threatcount))
        totalthreatcount = sum(self.conf.threatcount.values())

        def make_threat_turns() -> Mapping[str, Sequence[int]]:
            tc = self.conf.threatcount
            incidenttimes = randomgiven(self.conf.incidenttimesvalueshares,
                                        totalthreatcount)
            random.shuffle(incidenttimes)
            ret = {}
            doubles = [x for x in set(incidenttimes)
                       if incidenttimes.count(x) > 1]
            if len(doubles) > tc['internal'] + tc['serious_internal']:
                raise ValueError("Too many double threat times")
            if (len(set(incidenttimes))
                    < tc['internal'] + tc['serious_internal']):
                raise ValueError("Too few distinct threat times")
            ret['internal'] = doubles
            remaining = list(set(incidenttimes))
            while (len(ret['internal'])
                   < tc['internal'] + tc['serious_internal']):
                element = random.choice(remaining)
                while element in ret['internal']:
                    # terminates since we checked above
                    element = random.choice(remaining)
                ret['internal'].append(element)
                remaining.remove(element)
            random.shuffle(ret['internal'])
            if (ret['internal'] and min(ret['internal'])
                    <= self.conf.internaldelay):
                raise ValueError("Internal too early")
            zones = ['red', 'white', 'blue']
            random.shuffle(zones)
            for zone in itertools.cycle(zones):
                if not remaining:
                    break
                element = random.choice(remaining)
                ret.setdefault(zone, []).append(element)
                remaining.remove(element)
            for zone in ('red', 'white', 'blue', 'internal'):
                ret.setdefault(zone, []).sort()
                for x, y in zip(ret[zone], ret[zone][1:]):
                    if y - x <= self.conf.normalwaketurns:
                        raise ValueError("Too close together")
            return ret

        def mark_serious_and_unconfirmed(
                threatturns: Mapping[str, Sequence[int]]) -> Sequence[Threat]:
            tc = self.conf.threatcount
            thethreats = []
            seriousturns = []
            for _ in range(tc['serious_internal']):
                element = random.choice(self.eligible_serious(
                    threatturns['internal'], seriousturns))
                seriousturns.append(element)
                thethreats.append(Threat(None, element, ThreatKind.internal,
                                         unconfirmed=False, serious=True))
            for x in threatturns['internal']:
                if x not in seriousturns:
                    thethreats.append(Threat(None, x, ThreatKind.internal,
                                             unconfirmed=False, serious=False))
            externallog = []
            for _ in range(tc['serious_external']):
                zone = random.choice(('red', 'white', 'blue'))
                element = random.choice(self.eligible_serious(
                    threatturns[zone], seriousturns))
                seriousturns.append(element)
                externallog.append(element)
                thethreats.append(Threat(None, element, ThreatKind[zone],
                                         unconfirmed=False, serious=True))
            for zone in ('red', 'white', 'blue'):
                for x in threatturns[zone]:
                    if x not in externallog:
                        thethreats.append(Threat(
                            None, x, ThreatKind[zone], unconfirmed=False,
                            serious=False))
            accumulated = {i: sum((2 if x.serious else 1)
                                  for x in thethreats if x.turn <= i)
                           for i in range(1, 13)}
            for turn in accumulated:
                if accumulated[turn] > self.conf.maxthreatpointsinturn[turn]:
                    raise ValueError("Too much too early.")
            upoints = self.conf.unconfthreatpoints
            while upoints:
                onethreat = random.choice(tuple(
                    x for x in thethreats
                    if (not x.unconfirmed and (upoints > 1 or not x.serious))))
                onethreat.unconfirmed = True
                upoints -= 2 if onethreat.serious else 1
            random.shuffle(thethreats)
            return thethreats

        def distribute_threats() -> Sequence[Threat]:
            threatturns = try_repeatedly(make_threat_turns)
            if DEBUG:
                print("threatturns: {}".format(threatturns))
            return try_repeatedly(lambda: mark_serious_and_unconfirmed(
                threatturns))

        threats = try_repeatedly(distribute_threats)
        threats.sort(key=lambda x: x.turn)

        if DEBUG:
            print("threats : {}".format(threats))

        def place_threats(thethreats: Sequence[Threat]) -> None:
            for x in thethreats:
                x.time = None
            thethreats[0].time = self.conf.firstthreattime
            wake = (self.conf.seriouswaketime
                    if thethreats[0].serious else self.conf.normalwaketime)
            currenttime = (self.conf.firstthreattime
                           + EVENTDURATIONS["unconf_serious_threat"] + wake)
            thephase = 1
            cutoff = self.conf.phasethreatturns[thephase]
            lasttime = (self.conf.phaseduration[thephase]
                        - self.conf.protectedtime)
            for threat in thethreats[1:]:
                if DEBUG:
                    print("place threat {}".format(threat))
                thisphaseremaining = len(tuple(
                    x for x in thethreats
                    if x.turn <= cutoff and x.time is None))
                if not thisphaseremaining:
                    # we assume, that there is no phase without threats
                    # before a phase with threats
                    thephase += 1
                    currenttime = 10 + sum(
                        duration
                        for aphase, duration in self.conf.phaseduration.items()
                        if aphase < thephase)
                    lasttime += self.conf.phaseduration[thephase]
                    cutoff = self.conf.phasethreatturns[thephase]
                thisphaseremaining = len(tuple(
                    x for x in thethreats
                    if x.turn <= cutoff and x.time is None))
                threatlasttime = (
                    currenttime + (lasttime - currenttime)/thisphaseremaining)
                wake = (self.conf.seriouswaketime
                        if threat.serious else self.conf.normalwaketime)
                tmp = (threatlasttime - EVENTDURATIONS["unconf_serious_threat"]
                       - wake)
                threat.time = randombiased(currenttime, tmp)
                currenttime = (
                    threat.time + EVENTDURATIONS["unconf_serious_threat"]
                    + wake)

        try_repeatedly(lambda: place_threats(threats))

        for threat in threats:
            self.addevent(threat, EVENTDURATIONS["unconf_serious_threat"])

        def determine_commfailtimes() -> Sequence[int]:
            ret = []
            totalcommfailtime = randombiased(self.conf.mintotalcomfail,
                                             self.conf.maxtotalcomfail)
            while totalcommfailtime - sum(ret) > self.conf.maxsinglecomfail:
                ret.append(randombiased(self.conf.minsinglecomfail,
                                        self.conf.maxsinglecomfail))
            if totalcommfailtime - sum(ret) < self.conf.minsinglecomfail:
                raise ValueError("Comm fail too short")
            ret.append(totalcommfailtime - sum(ret))
            return ret

        commfailtimes = try_repeatedly(determine_commfailtimes)
        numdatatransfer = randomgivenone(self.conf.datatransfervalueshares)
        numincomingdata = randomgivenone(self.conf.incomingdatavalueshares)

        def find_extra_time(duration: float, free: Sequence[bool],
                            threshold: float = 0.0) -> int:
            point = random.randint(0, self.conf.totalduration)
            bar = random.random() * self.conf.totalduration
            if threshold * point > bar:
                raise ValueError("Too late")
            if not all(free[i] for i in range(point - 5,
                                              point + duration + 5)):
                raise ValueError("No spot for extra")
            return point

        def distribute_extras() -> Sequence[Event]:
            current_free = copy.deepcopy(self.free)
            ret = []
            for failtime in commfailtimes:
                dur = EVENTDURATIONS["comm_fail"] + failtime
                point = try_repeatedly(
                    lambda: find_extra_time(duration=dur, free=current_free),
                    maxtries=84)
                for i in range(point - 5, point + dur + 5):
                    current_free[i] = False
                ret.append(CommFail(point, failtime))
            for _ in range(numdatatransfer):
                point = try_repeatedly(
                    lambda: find_extra_time(
                        duration=EVENTDURATIONS["data_transfer"],
                        free=current_free, threshold=.25),
                    maxtries=168)
                for i in range(point - 5,
                               point + EVENTDURATIONS["data_transfer"] + 5):
                    current_free[i] = False
                ret.append(DataTransfer(point))
            for _ in range(numincomingdata):
                point = try_repeatedly(
                    lambda: find_extra_time(
                        duration=EVENTDURATIONS["incoming_data"],
                        free=current_free, threshold=.5),
                    maxtries=336)
                for i in range(point - 5,
                               point + EVENTDURATIONS["incoming_data"] + 5):
                    current_free[i] = False
                ret.append(IncomingData(point))
            return ret

        extras = try_repeatedly(distribute_extras)

        for event in extras:
            if isinstance(event, IncomingData):
                duration = EVENTDURATIONS["incoming_data"]
            elif isinstance(event, DataTransfer):
                duration = EVENTDURATIONS["data_transfer"]
            elif isinstance(event, CommFail):
                duration = EVENTDURATIONS["comm_fail"] + event.duration
            else:
                raise RuntimeError("Unrecognized extra event")
            self.addevent(event, duration)

    def setupphases(self) -> None:
        phasestart = 0
        phaseend = self.conf.phaseduration[1]
        self.addevent(PhaseTransition(phasestart, 0),
                      EVENTDURATIONS["operation_start"])
        s = self.repository[SK.first_phase_ends_in_1_minute]
        self.addevent(PhaseWarning(phaseend - 60 - s.duration, 1, 60),
                      EVENTDURATIONS["phase_warning"])
        s = self.repository[SK.first_phase_ends_in_20_seconds]
        self.addevent(PhaseWarning(phaseend - 20 - s.duration, 1, 20),
                      EVENTDURATIONS["phase_warning"])
        phasestart = phaseend
        phaseend += self.conf.phaseduration[2]
        self.addevent(PhaseTransition(phasestart - MARK, 1),
                      EVENTDURATIONS["phase_transition"])
        s = self.repository[SK.second_phase_ends_in_1_minute]
        self.addevent(PhaseWarning(phaseend - 60 - s.duration, 2, 60),
                      EVENTDURATIONS["phase_warning"])
        s = self.repository[SK.second_phase_ends_in_20_seconds]
        self.addevent(PhaseWarning(phaseend - 20 - s.duration, 2, 20),
                      EVENTDURATIONS["phase_warning"])
        phasestart = phaseend
        phaseend += self.conf.phaseduration[3]
        self.addevent(PhaseTransition(phasestart - MARK, 2),
                      EVENTDURATIONS["phase_transition"])
        s = self.repository[SK.operation_ends_in_1_minute]
        self.addevent(PhaseWarning(phaseend - 60 - s.duration, 2, 60),
                      EVENTDURATIONS["phase_warning"])
        s = self.repository[SK.operation_ends_in_20_seconds]
        self.addevent(PhaseWarning(phaseend - 20 - s.duration, 2, 20),
                      EVENTDURATIONS["phase_warning"])
        self.addevent(PhaseTransition(phaseend - MARK, 3),
                      EVENTDURATIONS["operation_end"])

    def addevent(self, event: Event, duration: float) -> None:
        if DEBUG:
            print("add event={} with length={}".format(event, duration))
        self.plan.append(event)
        for i in range(-2, int(duration) + 2):
            now = int(event.time) + i
            if now >= 0:
                if not self.free[now]:
                    if DEBUG:
                        print("free={}, plan={}".format(self.free, self.plan))
                    raise ValueError(
                        "Simultaneous planning at time {}".format(now))
                self.free[now] = False

    def makejson(self) -> None:
        output = dict()
        output['phasedurations'] = self.conf.phaseduration
        output['events'] = []
        for event in self.plan:
            theevent = dict()
            theevent["time"] = event.time
            if isinstance(event, Threat):
                theevent['type'] = "threat"
                theevent['turn'] = event.turn
                theevent['kind'] = event.kind.name
                theevent['unconfirmed'] = event.unconfirmed
                theevent['serious'] = event.serious
                output['events'].append(theevent)
            elif isinstance(event, DataTransfer):
                theevent['type'] = "datatransfer"
                output['events'].append(theevent)
            elif isinstance(event, IncomingData):
                theevent['type'] = "incomingdata"
                output['events'].append(theevent)
            elif isinstance(event, CommFail):
                theevent['type'] = "commfail"
                theevent['duration'] = event.duration
                output['events'].append(theevent)
            elif isinstance(event, PhaseWarning):
                pass
            elif isinstance(event, PhaseTransition):
                pass
            else:
                raise Exception("Unknown event type")
            output['events'].sort(key=lambda x: x['time'])
        jsonfile = open("mission.json", 'w')
        jsonfile.write(json.dumps(output, indent=4))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Plan a mission for Space Alert.')
    parser.add_argument('configpath', default='./config.cfg',
                        help='path/to/config/file', nargs='?')
    args = parser.parse_args()
    print("Initialize planner")
    random.seed()
    conf = Config(args.configpath)
    repository = SoundRepository()
    planner = Planner(conf, repository)
    print("Start planner")
    planner.makeaplan()
    print("Finished planning")
